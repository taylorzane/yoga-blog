#!/usr/bin/env bash

# TODO:
# - [ ] Add templating to automatically fill in the date of the new file.
# - [x] Automatically add day to README
#

today=$(date -j "+%Y-%m-%d")

today_file=$(ls| grep $today -m1)

if [ $today_file ] || false
then
  echo "You already did today! $today_file"
  exit 1
else
  days=$(ls | grep -Eo "^day[^x]*?-" --color=never | sed s/day//g | sed s/-//g)

  min=0 max=0

  for i in ${days[@]}; do
    (( $i > max || max == 0)) && max=$i
    (( $i < min || min == 0)) && min=$i
  done

  next=$((max+1))

  newfile="day$next-$today.md"

  cat dayx-temp-la-te.md > $newfile

  echo >> README.md
  echo "### [Day $next](https://gitlab.com/taylorzane/yoga-blog/blob/master/$newfile)" >> README.md

  echo "$newfile"
fi
