# Day 1 (2016-02-11)

- Routine: Wake -> Yoga -> Eat -> Work
- Wake Time: 08:00
- Yoga Time: 08:30
- Food: Yes -> 1 x Grapefruit
- Avg. BPM: 80BPM
- Energy Level: Low -> Medium
- Mood: Good -> Good
- Had Caffeine?: No
- Had Sugar?: No

[Back to README](https://gitlab.com/taylorzane/yoga-blog)
