# Overview

Hi my name is Taylor, and this is my blog about me doing yoga.

I plan to make a post every day logging energy levels, mood, heart rate, etc.

# Days

### [Day 1](https://gitlab.com/taylorzane/yoga-blog/blob/master/day1-2016-02-11.md)

### [Day 2](https://gitlab.com/taylorzane/yoga-blog/blob/master/day2-2016-02-12.md)

### [Day 3](https://gitlab.com/taylorzane/yoga-blog/blob/master/day3-2016-02-15.md)

### [Day 4](https://gitlab.com/taylorzane/yoga-blog/blob/master/day4-2016-02-16.md)

### [Day 5](https://gitlab.com/taylorzane/yoga-blog/blob/master/day5-2016-02-17.md)

### [Day 6](https://gitlab.com/taylorzane/yoga-blog/blob/master/day6-2016-02-18.md)

### [Day 7](https://gitlab.com/taylorzane/yoga-blog/blob/master/day7-2016-02-19.md)

### [Day 8](https://gitlab.com/taylorzane/yoga-blog/blob/master/day8-2016-02-23.md)

### [Day 9](https://gitlab.com/taylorzane/yoga-blog/blob/master/day9-2016-02-29.md)

### [Day 10](https://gitlab.com/taylorzane/yoga-blog/blob/master/day10-2016-03-03.md)

### [Day 11](https://gitlab.com/taylorzane/yoga-blog/blob/master/day11-2016-03-07.md)

### [Day 12](https://gitlab.com/taylorzane/yoga-blog/blob/master/day12-2016-03-23.md)

### [Day 13](https://gitlab.com/taylorzane/yoga-blog/blob/master/day13-2016-03-24.md)
