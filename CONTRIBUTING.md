# CONTRIBUTING

- TODO:
  - [ ] Finish the Day Format section...

Here are the guidelines for contributing:

1. Be you
1. Do yoga
1. Fork this
1. Run `generate-day.sh`
1. Input your data to new day
1. commit; push

All pull requests to insert day log(s) with be denied. If you wish to submit a PR to improve the day template file or the scripts, have at it.


# Day Format

- Data that is chained shall use an arrow `->`
  - e.g.: `Good -> Great`
- Data that is a Yes/No question shall use an arrow only if answered 'Yes'
  - e.g.: `Yes -> Foo`
  - e.g.: `No` (nothing else)

##### Current Template:

```markdown
# Day {{day}} ({{date}})

- Routine: Wake -> Yoga -> Eat -> Work
- Wake Time: 05:00
- Yoga Time: 05:30
- Food: Yes -> 1 x Food
- Avg. BPM: 75BPM
- Energy Level: Before -> After
- Mood: Before -> After
- Had Caffeine?: Yes -> 1 x Drink
- Had Sugar?: No

[Back to README](https://gitlab.com/taylorzane/yoga-blog)

```
