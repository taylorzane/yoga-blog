# Day {{day}} ({{date}})

- Routine: Wake -> Yoga -> Eat -> Work
- Wake Time: {{waketime}}
- Yoga Time: {{yogatime}}
- Food: Yes -> 1 x Food
- Avg. BPM: {{bpm}}BPM
- Energy Level: Before -> After
- Mood: Before -> After
- Had Caffeine?: Yes -> 1 x Drink
- Had Sugar?: No

[Back to README](https://gitlab.com/taylorzane/yoga-blog)
